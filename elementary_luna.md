#EnLab's Elementary OS Luna Installation Guide


<span style="color:#259640">Welcome to elementary OS. We have found this operating system to be the most rubust and user friendly linux distrobution for our needs.  This is a tutorial by [@WaltSpence](http://twitter.com/waltspence) of [@EnLabUSA](http://twitter.com/EnLabUSA) on how to get the most of elementary OS. To get an automated script of these steps visit [here](http://enlab.us/files/code/elementaryluna_fre.sh). Download the file and make the file executable by executing the following in terminal.</span>

- <span style="color:#259640">You can download via wget.</span>

>`wget http://enlab.us/files/code/elementaryluna_fre.sh && sudo chmod a+x elementaryluna_fre.sh` 

- <span style="color:#259640">If you've downloaded by visiting via the browser, you only need to make it executable.</span>

>`sudo chmod a+x elementaryluna_fre.sh` 

- <span style="color:#259640">Execute the script.</span>

>`./elementaryluna_fre.sh`


##First Things First
-----------------------------------------------------------

- **update elementary**
<span style="color:#259640">As with any operating system installation the first step is to update it. However, there are a few more essential steps to make elementary OS sing.</span>
>`sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y`  

- **Display hidden auto-start entries** 
<span style="color:#259640">This is sort of optional. Personally, I like to see everything that starts on my systems.</span>
>`sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop`  

- **Install elementary tweaks**
<span style="color:#259640">This is a very **essential** addition to System Settings (AKA Switchboard). Recommendations on Tweaks are to follow after 3rd party programs recommendations.</span>
>`sudo apt-add-repository ppa:versable/elementary-update -y && sudo apt-get update && sudo apt-get install elementary-tweaks`  

- **Install archiving utilities**
<span style="color:#259640">This is a very **essential** addition to the Operating system. Install via terminal using this command.</span>
>`sudo apt-get install unace rar unrar p7zip-rar p7zip zip unzip sharutils uudeview mpack lha arj cabextract file-roller unrar unrar-free -y`  

- **Install 3rd Party codecs**
<span style="color:#259640">Avoid annoying codecs issues when trying to play audio/video.</span>
>`sudo apt-get install gsfonts-x11 libxine1-ffmpeg gxine mencoder mpeg2dec vorbis-tools id3v2 mpg321 mpg123 libflac++6 ffmpeg totem-mozilla icedax tagtool easytag id3tool lame nautilus-script-audio-convert libmad0 libjpeg-progs flac faac faad sox ffmpeg2theora libmpeg2-4 uudeview flac libmpeg3-1 mpeg3-utils mpegdemux liba52-0.7.4-dev libquicktime2 gstreamer0.10-ffmpeg gstreamer0.10-fluendo-mp3 gstreamer0.10-gnonlin gstreamer0.10-sdl gstreamer0.10-plugins-bad-multiverse gstreamer0.10-plugins-bad gstreamer0.10-plugins-ugly totem-plugins-extra gstreamer-dbus-media-service gstreamer-tools ubuntu-restricted-extras -y`  

- **Enable DVD support**
<span style="color:#259640">CSS is also required by running the script.</span>
>`sudo apt-get install libdvdread4 && sudo /usr/share/doc/libdvdread4/install-css.sh`  

- **Install Fresh Player Plugin In Ubuntu Via PPA (Pepper Flash Wrapper For Firefox) **
<span style="color:#259640">Fresh player is a flash plugin replacement since adobe is no longer supporting flash in linux.</span>
>`sudo add-apt-repository ppa:nilarimogard/webupd8 -y &&
sudo apt-get update &&
sudo apt-get install freshplayerplugin -y`

- **Install Microsoft Core Fonts**
<span style="color:#259640">Ensure compatability with Microsoft fonts by installing this.</span>
>`sudo apt-get install ttf-mscorefonts-installer`

- **Install Google Fonts**
<span style="color:#259640">Give yourself even more font options by installing Google fonts.</span>
>`wget -O fonts.zip https://github.com/w0ng/googlefontdirectory/zipball/master && 
unzip fonts.zip && mv w0ng*/**/*.ttf ~/.fonts`

##Programs
-----------------------------------------------------------

<span style="color:#259640">These Programs are curated by EnLab and may not be specific to elementary OS. Additional depedencies **may** be required</span>

###Essential

<span style="color:#259640">We love these and couldn't deal without them.</span>

- **synaptic**             [package mangement tool](http://nongnu.org/synaptic/)
>`sudo apt-get install synaptic -y`

- **gdebi**                [package installer](https://apps.ubuntu.com/cat/applications/precise/gdebi/)
>`sudo apt-get install gdebi -y`

- **deja-dup**             [backup management tool](https://launchpad.net/deja-dup)
>`sudo apt-get install deja-dup -y`

- **firefox**              [mozilla's web browser](https://mozilla.org/en-US/firefox/)
>`sudo apt-get install firefox -y`

- **thunderbird**          [mozilla's e-mail client](https://mozilla.org/en-US/thunderbird)
>`sudo apt-get install thunderbird -y`

- **gedit**                [text editor](https://wiki.gnome.org/Apps/Gedit)
>`sudo apt-get install gedit -y`

- **clipit**               [clipboard manager](https://apps.ubuntu.com/cat/applications/clipit/)
>`sudo apt-get install clipit -y`

- **google chrome**        [web browser](http://google.com/chrome/)
>`sudo apt-get install libxss1 libappindicator1 libindicator7 -y && 
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && 
sudo dpkg -i google-chrome*.deb &&
sudo rm -r google-chrome*.deb -y`

- **vlc**                  [video player](http://videolan.org/vlc/)
<span style="color:#259640">Version 2.0.2 is available in the main repos. To install 2.1x see below.</span>
>`sudo apt-get install vlc -y`

<span style="color:#259640">Version 2.1x</span>
>`sudo add-apt-repository ppa:djcj/vlc-stable -y && 
sudo apt-get update && 
sudo apt-get install vlc`

- **transmission**         [bitorrent client](https://transmissionbt.com/)
>`sudo apt-get install transmission -y`

- **gimp**                 [image editor](http://gimp.org/)
<span style="color:#259640">Version 2.6 is available in the main repos. To install 2.8 see below</span>
>`sudo apt-get install gimp -y`

<span style="color:#259640">Version 2.8</span>
>`sudo add-apt-repository ppa:otto-kesselgulasch/gimp -y && 
sudo apt-get update && sudo apt-get install gimp -y && 
sudo apt-get install gimp-plugin-registry -y && 
sudo apt-get install gimp-gmic -y && 
sudo apt-get install gimp-help-en -y && 
sudo apt-get install gimp-lensfun -y`

- **gpicview**             [lightweight graphic viewer](http://lxde.sourceforge.net/gpicview/)
>`sudo apt-get install gpicview -y`

- **calibre**              [ebook management](http://calibre-ebook.com/)
>`sudo apt-get install calibre -y`

- **liferea**               [RSS aggregator](http://lzone.de/liferea/)
>`sudo apt-get install liferea -y`

- **brasero**             [disc burning utility](https://wiki.gnome.org/Apps/Brasero)
>`sudo apt-get install brasero -y`

- **Handbrake**	     [DVD copy tool](http://handbrake.fr/)
>`sudo apt-add-repository ppa:stebbins/handbrake-snapshots -y && 
sudo apt-get update && 
sudo apt-get install handbrake-gtk -y`

- **GnuCash**              [personal finance system](http://gnucash.org/)
>`sudo apt-get install gnucash -y`

- **LibreOffice**          [office suite](http://libreoffice.org/)
>`sudo apt-get install libreoffice -y`

- **System Monitor**          [gnome system monitor](http://gnome.org/)
>`sudo apt-get install gnome-system-monitor`

- **uGet**         	     [download manager](http://ugetdm.com/)
>`sudo add-apt-repository ppa:plushuang-tw/uget-stable &&
sudo apt-get update &&
sudo apt-get install uget`


###Optional

<span style="color:#259640">While we love these programs as well, we realize they may not be essential to you.</span>

- **filezilla**            [ftp client](https://filezilla-project.org/)
>`sudo apt-get install filezilla -y`

- **brackets**             [adobe's open source text editor that understands web design](http://brackets.io/)
>`sudo add-apt-repository ppa:webupd8team/brackets -y &&
sudo apt-get update &&
sudo apt-get install brackets -y`

- **retext**               [markdown editor](http://sourceforge.net/projects/retext/)
>`sudo apt-get install retext -y`

- **audacity**             [audio editor](http://audacity.sourceforge.net/)
>`sudo apt-get install audacity -y`

- **kino**                 [video editor](http://kinodv.org/)
>`sudo apt-get install kino -y`

- **VirtualBox**           [virtual machine management tool](https://virtualbox.org/)
>`sudo apt-get install virtualbox -y`

- **Xchat IRC**            [IRC Client](http://xchat.org/)
>`sudo apt-get install xchat -y`

- **Scribus**              [desktop publishing tool](http://scribus.net)
>`sudo apt-get install scribus -y`

- **PSensor**	     [CPU monitor](http://wpitchoune.net/blog/psensor/)
>`sudo apt-get install psensor -y`

- **Variety**	     [wallpaper manager](http://peterlevi.com/variety/)
>`sudo add-apt-repository ppa:peterlevi/ppa -y && 
sudo apt-get update && 
sudo apt-get install variety -y`

- **Pyroom**	     [distraction free word processor](http://pyroom.org/)
>`sudo apt-get install pyroom -y`

- **Steam Games**	     [steam videogames](http://store.steampowered.com/)
<span style="color:#259640">Note: I haven't tested this personally on elementary OS. It works well in Ubuntu and from what I read there are no issues in elementary OS Luna (version 0.2). I can't speak for compatability with the freya beta for this or any of the mentioned above.</span>
>`sudo apt-get install steam -y`

###Advanced tools

- **FreeFileSync**	     [directory comparison tool](http://sourceforge.net/projects/freefilesync/)
>`sudo add-apt-repository ppa:freefilesync/ffs -y &&
sudo apt-get update &&
sudo apt-get install freefilesync -y`

- **MenuLibre**	     [menu editing tool](http://smdavis.us/projects/menulibre/)
<span style="color:#259640">Note: You will first have to run this in terminal by typing in /opt/extras.ubuntu.com/menulibre/bin/menulibre. You can then add it to the menu after launching the program.</span>
>` sudo apt-add-repository ppa:versable/elementary-update -y &&
sudo apt-get update &&
sudo apt-get install menulibre -y`

- **Y-PPA Manager**	     [Personal Package Archives Manger](http://webupd8.org/2010/11/y-ppa-manager-easily-search-add-remove.html)
>`sudo add-apt-repository ppa:webupd8team/y-ppa-manager &&
sudo apt-get update &&
sudo apt-get install y-ppa-manager`

###Popular apps with known issues and Complex Installations

- ###Dropbox	     [file storage and synchronization service](http://dropbox.com/)  
<span style="color:#259640">First, the daemon works perfectly on elementary OS Luna with a caveat. The app indicator that should show in the wingpanel as it does in Windows system tray and OS X's top menu. However, it doesn't. There is a way to fix this bug if it irritates you (see below).</span>  

>###<span style="color:#259640">To install Dropbox on 64 bit systems-</span>

>`wget https://www.dropbox.com/download?dl=packages/ubuntu/dropbox*amd64.deb && 
sudo dpkg -i dropbox*amd64.deb && 
sudo rm -r dropbox*amd64.deb -y`

>###<span style="color:#259640">To install Dropbox on 32 bit systems-</span>

>`wget https://www.dropbox.com/download?dl=packages/ubuntu/dropbox*i386.deb && 
sudo dpkg -i dropbox*i386.deb && 
sudo rm -r dropbox*i386.deb -y`

>###<span style="color:#259640">To get the Dropbox app indicator to show:</span>

- **Open up Files and go to your Home folder**.

- **Right click and choose show hidden files**.

- **Look for a file named `.xsessionrc`**

    >If you have it the open it in Scratch.

    >If you don’t have it then right click and choose create new file > empty file, and name it `.xsessionrc` then open it in Scratch

- **In the file `.xsessionrc`**
>Paste in export DROPBOX_USE_LIBAPPINDICATOR=1

- **Close .xsessionrc**

- **Open the terminal and run the following commands one at a time.**

 >`cd`

 >`dropbox stop`

 >`rm .dropbox-dist`

 >`rm -R .dropbox-dist`

 >`sudo rm -R /usr/bin/.dropbox-dist/`

 >`sudo rm -R /var/lib/dropbox/.dropbox-dist`

 >`dropbox start -i`

- **To take it a step *further* and install prettier icons see the link [here](http://askubuntu.com/questions/18832/how-can-i-change-dropboxs-indicator-applet-icon)**

- **Spotify**	     [streaming music service](http://spotify.com/)
>`See [this guide](http://webupd8.org/2011/12/how-to-install-native-spotify-linux.html) from webupd8.`

##Tweaks
-----------------------------------------------------------

<span style="color:#259640">Here are a few tweaks to make your elementary OS experience even better.</span>

- **Turn off Touchpad clicks**  
<span style="color:#259640">I don't know why this is a thing in all operating systems.</span>
>`System Settings>Mouse and Touchpad>Touchpad>Untick Enable mouse clicks with Touchpad`

- **Check for Additional Drivers while you're at it...**  
<span style="color:#259640">Be mindful of drivers if any additional are available. Installing a driver could damage your install.</span>
>`System Settings>Additional Drivers`

- **Change System Default Apps after you've installed favorite apps**  
<span style="color:#259640">Note If the MIME Type isn't available in the switchboard panel here, then you can define it by right clicking on a file of its type in Files.</span>
>`System Settings>Defaults`  

- **Uninstall Ubuntu One**  
<span style="color:#259640">Until the release of freya, this ubuntu file sharing service that is now defunct will be on the system unless you uninstall it.</span>
>`sudo apt-get remove --purge ubuntuone-client -y`  

- **Turn off Single Click in Files**  
<span style="color:#259640">As optional as this is, it is very dangerous to use single click in a file manager.</span>
>`System Settings>Tweaks>Files>Toggle Single Click`  

- **Add shortcuts via elementary tweaks**  
<span style="color:#259640">Everyone loves keyboard shortcuts.</span>
>`System Settings>Tweaks>Shortcuts`  
><span style="color:#259640">One that I use personally is one for Files as seen here:</span>

<table>
<tr>
Name:         Launch Files
</tr>
<tr>
Shortcut:     CTRL+ALT+F
</tr>
<tr>
Command:      pantheon-files
</tr>
</table>

- **Deja-Dup to backup files**  
><span style="color:#259640">You can use the method of your choice. Personally I use the following configuration.</span>
><span style="color:#259640">Backup via SSH (port 22) @ IP:192.168.1.xxx or as ServerName1 as configured in `/etc/hosts`  (more on this below)</span>


##Configuration
-----------------------------------------------------------

<span style="color:#259640">As with any linux distro there are a couple of 
tweaks to conf files that can make the os even better.</span>

- Samba
<span style="color:#259640">Samba has a quirk in it that needs to be solved by editing...</span>

>`sudo nano /etc/samba/smb.conf`

<span style="color:#259640">...and appending the following line in the configuration file.</span>

>`name resolve order= bcast host`

<span style="color:#259640">You can download the version I have by running wget</span>

>`wget http://enlab.us/files/conf/smb.conf'


##Commands
-----------------------------------------------------------

###Essentials

- **Banish404**	     [automatically removes ppas that are 404](https://launchpad.net/~fossfreedom/+archive/ubuntu/packagefixes)
<span style="color:#259640">This is used in my project script. Heck, I use it all of the time.</span>
>`sudo add-apt-repository ppa:fossfreedom/packagefixes -y && 
sudo apt-get update && 
sudo apt-get install banish404 -y`  


##Developer CLI Utils
-----------------------------------------------------------

<span style="color:#259640">These are command line utilities that we find essential to our development workflow</span>

- **Git**	     [SCM](http://git-scm.com/)
>`sudo apt-get install git-core -y`

><span style="color:#259640">Then configure</span>

- `git config --global user.name "NewUser"`
- `git config --global user.email newuser@example.com`

**Node.js via NVM**


- **Bower**	     [package manager for the web built on node.js](http://bower.io/)
<span style="color:#259640">This is used in my project script. Heck, I use it all of the time.</span>
>`npm install -g bower`

- **PIP**	     [Python Package manager](https://pypi.python.org/pypi/pip)
>`sudo apt-get install python-pip -y`

- **Markdown CLI**	     [markdown command to turn md into html](http://manpages.ubuntu.com/manpages/lucid/man1/markdown.1.html)
>`sudo apt-get install markdown -y`


##Helpful Files
-----------------------------------------------------------

<span style="color:#259640">Here are some files we use that you may find helpful</span>

###EnLab .code  

- **Update**
<span style="color:#259640">an update bash script for lazy people/</span>
>`wget http://enlab.us/files/code/update`  

- **EnLabWebStructurer**
<span style="color:#259640">Web project creator written bash</span>
>`wget http://enlab.us/files/code/EnLabWebStructurer`


##More Helpful Information
-----------------------------------------------------------

<span style="color:#259640">Here are some helpful keyboard shortcuts.</span>

- Ctrl + T - Open a new tab in Files
- Ctrl + Alt + T - Starts terminal
- Alt + Tab - Switch between apps that are opened
- Superkay + A - Show all windows on every desktop
- Superkey + W - Show all windows on the current desktop
- Superkey + Tab - Switch between desktops (in order they are used)
- Superkey + directional arrows - Switch between desktops
- Superkey + 1,2,3,4,5 ... -Switch between desktops  