#Download Elementary OS from here: 
#http://sourceforge.net/projects/elementaryos/files/stable/

#read options
#case $options in
#    1) bower install https://github.com/twbs/bootstrap.git --silent&&mv bower_components/bootstrap assets/src/;;
#    2) bower install https://github.com/uikit/uikit.git --silent&&mv bower_components/uikit assets/src/;;
#    *) invalid option;;
#esac

read -r -p "Do you have the update script installed? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
	echo -e "$CONFIRMED"
    update
        ;;
    *)
        echo -e "Downloading"
wget http://enlab.us/files/update -q && sudo mv update /usr/bin/ && chmod a+x update && update
        ;;
esac


echo -e "select the design framework and press enter.\n 1)bootstrap\n 2)ui-kit"

#First you update your system
sudo apt-get update && sudo apt-get dist-upgrade
 
 
#Install Google Chrome & Firefox
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update
sudo apt-get install google-chrome-stable
sudo apt-get install firefox
 
#Clean-up System
sudo apt-get remove --purge midori-granite empathy geary noise
sudo apt-get autoremove
sudo apt-get autoclean
 
#Remove some Switchboard Plug's
sudo rm -rf /usr/lib/plugs/GnomeCC/gnomecc-bluetooth.plug
sudo rm -rf /usr/lib/plugs/GnomeCC/gnomecc-wacom.plug
 
#Install gedit (Text Editor)
sudo apt-get install gedit gedit-plugins
 
#Install File Compression Libs
sudo apt-get install unace unrar zip unzip xz-utils p7zip-full p7zip-rar sharutils rar uudeview mpack arj cabextract file-roller
 
 
#Install Ubuntu Restricted Extras
sudo apt-get install ubuntu-restricted-extras
 
#Enable Movie DVD Support
sudo apt-get install libdvdread4
sudo /usr/share/doc/libdvdread4/install-css.sh

#Install Dropbox
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -

## Caminho para executar pelo sistema:
~/.dropbox-dist/dropboxd

##Integracao Dropbox e ElementaryOS:
sudo apt-get install pantheon-files-plugin-dropbox
 
#Install a Firewall Application
sudo apt-get install gufw
 
#Install Elementary OS extras
sudo apt-add-repository ppa:versable/elementary-update
sudo apt-get update
 
sudo apt-get install elementary-desktop elementary-tweaks
sudo apt-get install elementary-dark-theme elementary-plastico-theme elementary-whit-e-theme elementary-harvey-theme elementary-lion-theme
sudo apt-get install elementary-wallpaper-colection
sudo apt-get install elementary-elfaenza-icons elementary-nitrux-icons
sudo apt-get install elementary-plank-themes
sudo apt-get install wingpanel-slim indicator-synapse
 
 
#Install Java 7
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer
 
#Install Steam
wget http://repo.steampowered.com/steam/signature.gpg && sudo apt-key add signature.gpg
sudo sh -c 'echo "deb http://repo.steampowered.com/steam/ precise steam" >> /etc/apt/sources.list.d/steam.list'
sudo apt-get update
sudo apt-get install steam
 
#Install Skype
sudo apt-add-repository "deb http://archive.canonical.com/ubuntu/ precise partner"
sudo apt-get update && sudo apt-get install skype
 
#Install the latest git Version
sudo add-apt-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install git
 
#Install the latest Version of VirtualBox
wget -q http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc -O- | sudo apt-key add -
sudo sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian precise contrib" >> /etc/apt/sources.list.d/virtualbox.list'
sudo apt-get update
sudo apt-get install virtualbox-4.3
 
# Install Nodejs and NPM via PPA
sudo apt-get update && sudo apt-get install -y python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update && sudo apt-get install nodejs
 
# Install Apache2, PHP5, MySQL and PHPMyAdmin
sudo apt-get install apache2 mysql-server mysql-client php5 php-fpm* phpmyadmin
sudo apt-get update
sudo apt-get install curl libcurl3 libcurl3-dev php5-curl
 
# Install Ajenti.org
sudo wget http://repo.ajenti.org/debian/key
sudo apt-key add key
sudo scratch-text-editor /etc/apt/sources.list

# Add to last list:
http://repo.ajenti.org/ng/debian main main ubuntu
sudo apt-get update && sudo apt-get install ajenti
sudo service ajenti restart

#Install Sublime Text 3 via ppa
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install sublime-text-installer

# Conky - more info here: http://zagortenay333.deviantart.com/art/Conky-eOS-402533520?q=gallery%3Azagortenay333&qo=0
sudo add-apt-repository ppa:noobslab/noobslab-conky
sudo apt-get update
sudo apt-get install eos-conky

# Variety - Dynamic Wallpaper - more info here: http://peterlevi.com/variety/how-to-install/
wget http://ubuntuone.com/0mqnbkvE1xABvanVlcEQnQ -O peterlevi-ppa-precise.list
sudo mv peterlevi-ppa-precise.list /etc/apt/sources.list.d/
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A546BE4F
sudo apt-get update
sudo apt-get install variety
