#!/bin/bash
# ------------------------------------------------------------------
# [@WaltSpence of @EnLabUSA] elementary_lunafre.sh
#          Installs the software as recommended in the EnLab's Elementary OS
#	   Luna Installation Guide
#
#	   This is licensed under the Unlicense as seen at this link
#          http://unlicense.org/UNLICENSE
# ------------------------------------------------------------------

VERSION=0.1.0
SUBJECT=ELC-1214-005

# ------------------------------------------------------------------
PROGRAM=test
CONFIRMED="\n Awesome. \n Here we go..."

echo -e "\n   Hello $USER!\n\n\n		Welcome to Elementary Fre.sh\n		by EnLab,LLC | http://enlab.co\n                  @EnLabUSA      \n\n  For more information see http://enlab.us/elementary_luna/ \n\n "

#begin to make structure
mkdir -p ~/elementaryOS/{info/{manifest,src,vid,priv},doc/{api,system,project/{id,monstra-cms,font-awesome}}}

read -r -p "Are we installing $PROGRAM ? " response
case $response in
    [yY][eE][sS]|[yY])
	echo -e "$CONFIRMED"
    
    echo "[Desktop Entry]
    Encoding=UTF-8
    Name=Link to $PROGRAM website
    Type=Link
    URL=http://test.com/
    Icon=text-html" >> ~/elementaryOS/manifest/$PROGRAM/info/manifest/test.desktop
        ;;
    *)
        ;;
esac
echo -e "All installed packages are available in the manifest. (Under ~/elementaryOS/manifest/InstalledPackages.eos)"
dpkg --get-selections | grep -v deinstall > ~/elementaryOS/info/manifest/InstalledPackages.eos
